FROM python:3.6.4
WORKDIR /code
ADD . .
RUN apt-get update 
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD ["python3", "app.py"]