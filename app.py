from flask import Flask, render_template, url_for, request, session, redirect, flash, json
from flask_pymongo import PyMongo
from google.oauth2 import id_token
from google.auth.transport import requests
import os
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)
app.config['SECRET_KEY'] = os.getenv("SECRET")
app.config['MONGO_DBNAME'] = os.getenv("DB")
app.config['MONGO_URI'] = os.getenv("MONGODB")
mongo = PyMongo(app)

@app.route('/')
def index():
    if 'username' in session:
      login_user = getLoggedUserDetails()
      social = checksociallogin(login_user)
      if(isadmin()):
        return render_template('adminhome.html',social=social)
      else:
        show = checkisvoted(login_user)
        return render_template('userhome.html',social=social,show=show)
    return render_template('index.html')

@app.route('/checkResults')
def checkResults():
  if(isadmin()):
    results = checkresults()
    if(type(results) is list):
      data_found  = 'Yes'
    else:
      data_found  = 'No'
    return render_template('results.html',show=results,data_found=data_found)

@app.route('/google', methods=['POST'])
def google():
  CLIENT_ID=os.getenv("GOOGLE_CLIENT")
  try:
    idinfo = id_token.verify_oauth2_token(request.form.get('idtoken'), requests.Request(), CLIENT_ID)
    if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
        raise ValueError('Wrong issuer.')
    userid = idinfo['sub']
    users = mongo.db.user_login
    session['username'] = request.form.get('email')
    existing_user = users.find_one({'name' : request.form.get('email')})
    if existing_user is None:
      users.insert({'name' : request.form.get('email'),'role':'user','sociallogin':'Yes','dob':'','is_voted':''})
      users = mongo.db.user_login
      show = checkisvoted(existing_user)
      return render_template('userhome.html',show=show)
    else:
      if 'username' in session:
          login_user = getLoggedUserDetails()
          social = checksociallogin(login_user)
          role=login_user['role']
          if(role == 'admin'):
            results = checkresults()
            return render_template('adminhome.html',social=social,show=results)
          if(role == 'user'):
            show = checkisvoted(login_user)
            return render_template('userhome.html',social=social,show=show)
      return render_template('index.html')
  except ValueError:
    pass
  return 'success'

@app.route('/adminhome')
def adminhome():
  if(not isadmin()):
        return redirect(url_for('index'))
  login_user = getLoggedUserDetails()
  social = checksociallogin(login_user)
  results = checkresults()
  return render_template('adminhome.html',social=social,show=results)

@app.route('/userhome')
def userhome():
  login_user = getLoggedUserDetails()
  social = checksociallogin(login_user)
  show = checkisvoted(login_user)
  return render_template('userhome.html',social=social,show=show)

@app.route('/login', methods=['POST'])
def login():
    users = mongo.db.user_login
    query = {'name' : request.form['username']}
    login_user = users.find_one(query)
    if login_user:
        if request.form['pass'] == login_user['password']:
            session['username'] = request.form['username']
            return redirect(url_for('index'))
    flash('Invalid username/password ')
    return redirect(url_for('about'))

@app.route('/about')
def about():
  return render_template("about.html")

@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        users = mongo.db.user_login
        query = {'name' : request.form['username']}
        existing_user = users.find_one(query)
        if(request.form['username'] =='' or request.form['pass']==''):
          flash('username/password should not be empty')
          return redirect(url_for('about'))
        else:
          if existing_user is None:
              users.insert({'name' : request.form['username'], 'password' :request.form['pass'],'role':'user','sociallogin':'No','dob':'','is_voted':''})
              session['username'] = request.form['username']
              return redirect(url_for('index'))
          flash('User already exists!')
          return redirect(url_for('about'))
    return render_template('register.html')


@app.route('/logout')
def logout():
    session.pop('username')
    return redirect(url_for('index'))

@app.route('/fillform',methods=['POST','GET'])
def fillform():
  users = mongo.db.user_login
  if request.method == 'POST':
    if 'username' in session: 
      query = {'name' : session['username']}
      updaterole = { "$set": { "firstname": request.form['firstname'],"lastname": request.form['lastname'],"dob": request.form['dob'],"address": request.form['address'] } }
      users.update_one(query,updaterole)
      login_user1 = getLoggedUserDetails()
      show = checkisvoted(login_user1)
      social = checksociallogin(login_user1)
      return render_template('userhome.html',social=social,show=show)
  else:
    if 'username' in session: 
      login_user1 = getLoggedUserDetails()
      social = checksociallogin(login_user1)
      if('dob' in login_user1 and login_user1['dob'] == ''):
        return render_template('fillform.html',social=social,show='show')
      else:
        return render_template('fillform.html',social=social,show='No')

@app.route('/graphanalysis',methods=['GET'])
def graphanalysis():
  dataPoints = json.dumps(preparegraph())
  return render_template('graphanalys.html',dataPoints=dataPoints)

@app.route('/vote',methods=['GET'])
def vote():
  login_user1 = getLoggedUserDetails()  
  if 'username' in session:
    candidates = os.getenv("CANDIDATES").split(',')
    social = checksociallogin(login_user1)
    if(login_user1['is_voted']):
      return render_template("vote.html",social=social,candidates=candidates,show='voted')
    else:
      return render_template("vote.html",social=social,candidates=candidates,show='not_voted')

@app.route('/savevote',methods=['POST'])
def savevote():
  login_user1 = getLoggedUserDetails()
  if request.method == 'POST':
    if 'username' in session:
      social = checksociallogin(login_user1)
      cand = mongo.db.candidate
      users = mongo.db.user_login
      check = cand.find_one({'name':request.form['option']})
      if(login_user1['is_voted'] == ''):
        if(check != ''):
          query = {'name' : request.form['option']}
          updaterole = { "$set": { "count": check['count'] + 1,"persons_voted": check['persons_voted'].append(session['username'])} }
          user_query = {'name' : session['username']}
          updateuser = { "$set": { "is_voted": True} }
          users.update_one(user_query,updateuser)
          cand.update_one(query,updaterole)
          return render_template("vote.html",social=social,show='success')
      if(login_user1['is_voted']):
        return render_template("vote.html",social=social,show='voted')
      else:
        return render_template('userhome.html',social=social,show='Yes')

def isadmin():
  login_user = getLoggedUserDetails()
  if(login_user['role'] == 'admin'):
    return True
  else:
    return False

def addcandidate():
  already_done = 0
  if(not already_done):
    cand = mongo.db.candidate
    candidates = os.getenv("CANDIDATES").split(',')
    data ={'count':0,'persons_voted':[]}
    if candidates:
      for candiate in candidates:
        check = cand.count({'name':candiate})
        data['name'] = candiate
        if data and check == 0:
          cand.insert(data)
        data = {'count':0,'persons_voted':[]}
      already_done = 1
      return 'candidate data added successfully to db'
    else:
      return 'provided empty candidates'

def preparegraph():
  dataPoints = []
  cand_data = {}
  data = checkresults()
  for cand in data:
    cand_data['y'] = int(cand['percentage'])
    cand_data['label'] = str(int(cand['percentage']))+'%'
    cand_data['indexLabel'] = cand['name']
    dataPoints.append(cand_data)
    cand_data = {}
  return dataPoints
  
def getLoggedUserDetails():
  if 'username' in session:
    users = mongo.db.user_login
    query = {'name' : session['username']}
    login_user = users.find_one(query)
    return login_user

def checksociallogin(login_user):
  if('sociallogin' in login_user and login_user['sociallogin'] == "Yes"):
    return "Yes"
  else:
    return 'No'

def checkisvoted(login_user):
  if('dob' in login_user and login_user['dob'] != '' and login_user['is_voted'] == ''):
    show = 'Yes'
  elif(login_user['is_voted']):
    show = 'voted'
  else:
    show = 'No'
  return show

def checkresults():
  if(isadmin()):
    total_votes = 0
    result = []
    cand_data = {}
    cand = mongo.db.candidate
    pipeline = [
      {
       '$group':
         {
           '_id':'null',
           'totalAmount': { '$sum': "$count" },
           'count': { '$sum': 1 }
         }
     }]
    total_votes = list(cand.aggregate(pipeline))[0]['totalAmount']
    if(total_votes > 0):
      cursor = cand.find({'count':{'$ne':0}})
      for user in cursor:
        cand_data['votes'] = user['count']
        cand_data['percentage'] = (user['count'] / total_votes)*100
        cand_data['name'] = user['name']
        result.append(cand_data)
        cand_data = {}
      return result
    else:
      return 'No data found'

if __name__ == "__main__":
  addcandidate()
  app.run(debug=True)